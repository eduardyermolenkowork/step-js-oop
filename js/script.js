
class Visit {
    constructor(visitTarget, visitDate, fullName){
        this._visitTarget = visitTarget;
        this._visitDate = visitDate;
        this._fullName = fullName;
    }
}
class CardiologistVisit extends Visit {
    constructor(visitTarget, visitDate, fullName, age, pressure, bodyMassIndex, diseases, comment){
        super(visitTarget, visitDate, fullName);
        this._pressure = pressure;
        this._bodyMassIndex = bodyMassIndex;
        this._diseases = diseases;
        this._age = age;
        this._comment = comment;
    }

    get visitTarget() {
        return this._visitTarget;
    }

    get visitDate() {
        return this._visitDate;
    }

    get fullName() {
        return this._fullName;
    }

    get pressure() {
        return this._pressure;
    }

    get bodyMassIndex() {
        return this._bodyMassIndex;
    }

    get diseases() {
        return this._diseases;
    }

    get age() {
        return this._age;
    }
    get comment() {
        return this._comment;
    }
    get visitType() {
        return "Кардиолог"
    }
    toString() {
        return `{"fullName":"${this._fullName}",
        "visitDate":"${this._visitDate}",
        "visitTarget":"${this._visitTarget}", 
        "pressure":"${this._pressure}", 
        "bodyMassIndex":"${this._bodyMassIndex}", 
        "diseases":"${this._diseases}", 
        "age":"${this._age}", 
        "comment":"${this._comment}", 
        "visitType":"${this.visitType}"}`;
    }
}
class TherapistVisit extends Visit {
    constructor(visitTarget,  visitDate, fullName, age, comment){
        super(visitTarget, visitDate, fullName);
        this._age = age;
        this._comment = comment;
        this._visitTarget = visitTarget;
        this._visitDate = visitDate;
        this._fullName = fullName;
    }
    get visitTarget() {
        return this._visitTarget;
    }

    get age() {
        return this._age;
    }

    get visitDate() {
        return this._visitDate;
    }

    get fullName() {
        return this._fullName;
    }

    get comment() {
        return this._comment;
    }
    get visitType() {
        return "Терапевт"
    }
    toString() {
        return `{"visitTarget":"${this._visitTarget}",
                "age":"${this._age}",
                "visitDate":"${this._visitDate}",
                "fullName":"${this._fullName}",
                "visitType":"${this.visitType}",
                "comment":"${this._comment}"}`
    }
}
class DentistVisit extends Visit {
    constructor(visitTarget, visitDate, fullName, comment) {
        super(visitTarget, visitDate, fullName);
        this._comment = comment;
        this._visitTarget = visitTarget;
        this._visitDate = visitDate;
        this._fullName = fullName;
    }
    get visitTarget() {
        return this._visitTarget;
    }

    get visitDate() {
        return this._visitDate;
    }

    get fullName() {
        return this._fullName;
    }

    get comment() {
        return this._comment;
    }
    get visitType() {
        return "Стоматолог"
    }
    toString() {
        return `{"visitTarget":"${this._visitTarget}",
                "visitDate":"${this._visitDate}",
                "fullName":"${this._fullName}",
                "comment":"${this._comment}",
                "visitType":"${this.visitType}"}`
    }
}
let appStorage = [];
(function () {
    if(localStorage.getItem("card")) {
        const arrTmp = JSON.parse(`[${localStorage.getItem("card")}]`);
        for (let i = 0; i < arrTmp.length; i++) {
            builderVisit(
                arrTmp[i].visitType,
                [   arrTmp[i].visitTarget,
                    arrTmp[i].pressure,
                    arrTmp[i].bodyMassIndex,
                    arrTmp[i].diseases,
                    arrTmp[i].age,
                    arrTmp[i].visitDate,
                    arrTmp[i].fullName]
                .filter((val)=>val),
                arrTmp[i]["comment"]);
        }
    }
}());
const inputsHTML = {
    "Кардиолог": `
    <div class="wrapper-span">
        <span class="modal-text">Цель визита</span>
        <input value="Сердце" id="visitTarget" type="text">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">Обычное давление</span>
        <input value="144/80" id="pressure" type="text" placeholder="Систолическое/Диастолическое">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">Индекс массы тела, кг/м<sup>2</sup></span>
        <input value="5.5" id=bodyMassIndex" type="text">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">Перенесенные заболевания <br> сердечно-сосудистой системы</span>
        <input value="Аритмия" id="diseases" type="text">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">Возраст</span>
        <input value="54" id="age" type="number">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">Дата последнего визита</span>
        <input id="visitDate" type="date" value="2019-05-22">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">ФИО</span>
        <input value="Жмишенко Валерий Альбертовичь" id="fullName" type="text" placeholder="Фамилия Имя Отчество">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">Комментарий*</span>
        <textarea id="comment">bla bla bla</textarea>
    </div>
    <div class="modal-single-text" id="modal-single-text">
        * - поле необязательное дла ввода
    </div>
`,
    "Стоматолог": `
    <div class="wrapper-span">
        <span class="modal-text">Цель визита</span>
        <input id="visitTarget" type="text" value="зубы">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">Дата последнего визита</span>
        <input id="visitDate" type="date" value="2019-05-22">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">ФИО</span>
        <input id="fullName" value="Жмишенко Валерий Альбертовичь" type="text" placeholder="Фамилия Имя Отчество">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">Комментарий*</span>
        <textarea id="comment"></textarea>
    </div>
    <div class="modal-single-text" id="modal-single-text">
        * - поле необязательное дла ввода
    </div>`,
    "Терапевт": `
    <div class="wrapper-span">
        <span class="modal-text">Цель визита</span>
        <input id="visitTarget" type="text" value="патау, додик">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">Возраст</span>
        <input id="age" type="number" value="54">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">Дата последнего визита</span>
        <input id="visitDate" type="date" value="2019-05-22">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">ФИО</span>
        <input id="fullName" value="Жмишенко Валерий Альбертовичь" type="text" placeholder="Фамилия Имя Отчество"">
    </div>
    <div class="wrapper-span">
        <span class="modal-text">Комментарий*</span>
        <textarea id="comment"></textarea>
    </div>
    <div class="modal-single-text" id="modal-single-text">
        * - поле необязательное дла ввода
    </div>`
};
function createPrimaryFields(visit) {
    return {
        'ФИО: ' : visit.fullName,
        'Врачь: ' : visit.visitType
    }
}
function createSecondaryFields(visit) {
    switch (visit.constructor.name) {
        case "CardiologistVisit" : {
            return {
                'Цель визита: ' : visit.visitTarget,
                'Обычное давление: ' : visit.pressure,
                'Индекс массы тела, кг/м<sup>2</sup>: ' : visit.bodyMassIndex,
                'Перенесенные заболевания <br> сердечно-сосудистой системы:' : visit.diseases,
                'Возраст, лет: ' : visit.age,
                'Дата последнего визита: ' : visit.visitDate,
                'Комментарий: ' : visit.comment
            }
        }
        case "TherapistVisit" : {
            return {
                'Цель визита: ' : visit.visitTarget,
                'Возраст, лет: ' : visit.age,
                'Дата последнего визита: ' : visit.visitDate,
                'Комментарий: ' : visit.comment
            }
        }
        case "DentistVisit" : {
            return {
                'Цель визита: ' : visit.visitTarget,
                'Дата последнего визита: ' : visit.visitDate,
                'Комментарий: ' : visit.comment
            }
        }
    }

}
function builderVisitCard(index, primaryFields, secondaryFields) {
    const card = document.createElement("div");
    card.classList.add("card");
    card.id = `card-visit-${index + 1}`;
    const cardWrapper = document.createElement("div");
    cardWrapper.classList.add("card-wrapper");
    const cardHeaderWrapper = document.createElement("div");
    cardHeaderWrapper.classList.add("card-header-wrapper");
    cardHeaderWrapper.innerHTML =
        `<span class="card-header">Карта №${index + 1}</span>`;
    const closeBtn = document.createElement('i');
    closeBtn.classList.add("far", "fa-times-circle", "card-btn-close");
    closeBtn.addEventListener('click', ()=>{
        if(document.getElementById("deck").children.length === 1 && !document.getElementById("message")){
            const message = document.createElement('span');
            message.textContent = "не создано ни одного приема...";
            message.classList.add("message");
            message.id = "message";
            document.getElementById("deck").appendChild(message);
        }
        localStorage.setItem("card",
            appStorage.filter((val, ind) => {
                return ind !== [].indexOf.call(card.parentNode.children, card)
            }).toString());
        card.remove();
    });
    cardHeaderWrapper.appendChild(closeBtn);
    cardWrapper.appendChild(cardHeaderWrapper);
    for (let key in primaryFields) {
        const cardText = document.createElement('div');
        cardText.classList.add("card-text-left");
        const nameOfField = document.createElement('span');
        nameOfField.innerHTML = key;
        const dataField = document.createElement('span');
        dataField.classList.add('card-data');
        dataField.innerText = primaryFields[key];
        cardText.appendChild(nameOfField);
        cardText.appendChild(dataField);
        cardWrapper.appendChild(cardText);
    }
    const moreDetailsBtn = document.createElement('a');
    moreDetailsBtn.classList.add('card-more-details');
    moreDetailsBtn.innerText = 'Подробней...';
    moreDetailsBtn.addEventListener(("click"), ()=>{
        const moreDetailsWrapper = document.createElement('div');
        for (let key in secondaryFields) {
            const cardText = document.createElement('div');
            cardText.classList.add("card-text-left");
            const nameOfField = document.createElement('span');
            nameOfField.innerHTML = key;
            const dataField = document.createElement('span');
            dataField.classList.add('card-data');
            dataField.innerText = secondaryFields[key];
            cardText.appendChild(nameOfField);
            cardText.appendChild(dataField);
            moreDetailsWrapper.appendChild(cardText);
        }
        cardWrapper.appendChild(moreDetailsWrapper);
        const toggleMoreDetailsBtn = document.createElement('a');
        toggleMoreDetailsBtn.textContent = "Скрыть подробную информацию";
        toggleMoreDetailsBtn.classList.add('card-toggle-details');
        toggleMoreDetailsBtn.addEventListener("click", ()=>{
            moreDetailsWrapper.remove();
            cardWrapper.appendChild(moreDetailsBtn);
            toggleMoreDetailsBtn.remove();
        });
        cardWrapper.appendChild(toggleMoreDetailsBtn);
        moreDetailsBtn.remove();
    });
    cardWrapper.appendChild(moreDetailsBtn);
    card.appendChild(cardWrapper);
    //=====DRAG'N'DROP=====//
    function move(e) {
        card.style.transform = `translate(${e.clientX - card.mousePositionX}px,${e.clientY - card.mousePositionY}px)`;
    }

    card.addEventListener('mousedown',(e)=>{
        if (card.style.transform) {
            const transforms = card.style.transform;
            const transformX = parseFloat(transforms.split('(')[1].split(',')[0]);
            const transformY = parseFloat(transforms.split('(')[1].split(',')[1]);
            console.log(transformX, transformY);
            card.mousePositionX = e.clientX - transformX;
            card.mousePositionY = e.clientY - transformY;
        } else {
            card.mousePositionX = e.clientX;
            card.mousePositionY = e.clientY;
        }
        card.addEventListener('mousemove',move);
    });

    card.addEventListener('mouseup', e => {
        card.removeEventListener('mousemove',move);
    });

    return card;
}
/*=========Fnc() CREATE MODAL WINDOW===============*/
function createModalWindow() {
    ////////////////////////////////////////
    const overlayWithModal = document.createElement("div");
    const modal = document.createElement("div");
    const modalContainer = document.createElement("div");
    overlayWithModal.classList.add("fixed-overlay");
    overlayWithModal.id = "fixed-overlay";
    modal.classList.add("modal");
    modal.id = "modal";
    modalContainer.classList.add("modal_container");
    ////////////////////////////////////////
    const btnCloseWrapper = document.createElement("div");
    btnCloseWrapper.classList.add("wrapper-btn-close");
    btnCloseWrapper.innerHTML = "<i id='btn-close-modal' class=\"fas fa-times btn-close-modal\"></i>";
    const wrapperSpan = document.createElement("div");
    wrapperSpan.classList.add("wrapper-span");
    const modalTitle = document.createElement("span");
    modalTitle.classList.add("modal-text");
    modalTitle.textContent = 'Выберете врача';
    const modalSelect = document.createElement("select");
    modalSelect.innerHTML = `
        <option value="Кардиолог">Кардиолог</option>
        <option value="Стоматолог">Стоматолог</option>
        <option value="Терапевт">Терапевт</option>`;
    modalSelect.id = "select-list";
    modalSelect.addEventListener("change", (e)=>{
        document.getElementById("wrapper-inputs").innerHTML = inputsHTML[e.currentTarget.value]
    });
    const modalInputsWrapper = document.createElement("div");
    modalInputsWrapper.id = "wrapper-inputs";
    const modalBtnAppendWrapper = document.createElement("div");
    modalBtnAppendWrapper.classList.add("modal-btn-append-wrapper");
    const btnAppend = document.createElement("a");
    btnAppend.textContent = "создать заявку";
    btnAppend.classList.add("create-btn", "btn-append");
    btnAppend.id = "btn-append";
    modalBtnAppendWrapper.appendChild(btnAppend);
    //=====ASSEMBLING======//
    wrapperSpan.appendChild(modalTitle);
    wrapperSpan.appendChild(modalSelect);
    modalInputsWrapper.innerHTML = inputsHTML["Кардиолог"];
    modalContainer.appendChild(btnCloseWrapper);
    modalContainer.appendChild(wrapperSpan);
    modalContainer.appendChild(modalInputsWrapper);
    modalContainer.appendChild(modalBtnAppendWrapper);
    overlayWithModal.appendChild(modal).appendChild(modalContainer);
    return overlayWithModal;
}
/*=========Fnc() VALIDATE ALL FIELDS IN INPUTS=====*/
function isValidateFields(data){
    let isValidate = true;
    for (let i = 0; i < data.length; i++) {
        if(data[i].value === ""){
            return isValidate = false;
        }
    }
    return isValidate;
}
/*=========Fnc() Builder of Classes=================*/
function builderVisit(doctor, dataFields, comment) {
    if(!comment) {
        comment = "-//-";
    }
    switch (doctor) {
        case "Кардиолог" : {
            const [visitTarget, pressure, bodyMassIndex, diseases, age, visitDate, fullName]
                = [...dataFields].map((val)=>{
                return val.constructor.name ==="HTMLInputElement" ? val.value : val
            });
            const cardioVisit = new CardiologistVisit(
                visitTarget, visitDate, fullName, age, pressure, bodyMassIndex, diseases, comment
            );
            if(document.getElementById("message")){
                document.getElementById("message").remove();
            }
            document.getElementById("deck")
                .appendChild(
                    builderVisitCard(
                        document.querySelectorAll(".card").length,
                        createPrimaryFields(cardioVisit),
                        createSecondaryFields(cardioVisit)
                    ));
            appStorage.push(cardioVisit);
            localStorage.setItem("card", appStorage.toString());
            break;
        }
        case "Стоматолог" : {
            const [visitTarget, visitDate, fullName] = [...dataFields].map((val) => {
                return val.constructor.name ==="HTMLInputElement" ? val.value : val
            });
            const dentistVisit = new DentistVisit(visitTarget, visitDate, fullName, comment);
            if(document.getElementById("message")){
                document.getElementById("message").remove();
            }
            document.getElementById("deck")
                .appendChild(
                    builderVisitCard(
                        document.querySelectorAll(".card").length,
                        createPrimaryFields(dentistVisit),
                        createSecondaryFields(dentistVisit)
                    ));
            appStorage.push(dentistVisit);
            localStorage.setItem("card", appStorage.toString());
            break;
        }
        case "Терапевт" : {
            const [visitTarget, age, visitDate, fullName] = [...dataFields].map((val) => {
                return val.constructor.name ==="HTMLInputElement" ? val.value : val
            });
            const therapistVisit = new TherapistVisit(visitTarget, visitDate, fullName, age, comment);
            console.log(therapistVisit);
            if(document.getElementById("message")){
                document.getElementById("message").remove();
            }
            document.getElementById("deck")
                .appendChild(
                    builderVisitCard(
                        document.querySelectorAll(".card").length,
                        createPrimaryFields(therapistVisit),
                        createSecondaryFields(therapistVisit)
                    ));
            appStorage.push(therapistVisit);
            localStorage.setItem("card", appStorage.toString());
            break;
        }
    }
}
/*========DOM============*/
const btnCreateModal = document.getElementById("create-btn");
btnCreateModal.addEventListener("click", ()=>{
    const modalWindow = createModalWindow();
    const body = document.getElementsByTagName("BODY")[0];
    body.appendChild(modalWindow);
    modalWindow.addEventListener("click", (evt) => {
      if(evt.target === evt.currentTarget) {
          modalWindow.remove()
      }
    });
    document.getElementById("btn-close-modal").addEventListener("click", ()=>{
        modalWindow.remove()
    });
    document.getElementById("btn-append").addEventListener("click", () => {
        const doctor = document.getElementById("select-list").value;
        const dataFields = document.querySelectorAll("#wrapper-inputs > .wrapper-span > input");
        const comment = document.getElementById("comment").value;
        if(isValidateFields(dataFields)) {
            builderVisit(doctor, dataFields, comment);
            modalWindow.remove();
        } else {
            const singleTextSpan = document.getElementById("modal-single-text");
            const redText = document.createElement("span");
            redText.classList.add("modal-single-red-text");
            redText.innerHTML = "<br>заполните все обязательные поля";
            if(!document.querySelector(".modal-single-red-text")){
                singleTextSpan.appendChild(redText);
            }
        }
    })});
